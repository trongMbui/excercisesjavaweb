package com.example.ovningsuppgifter;

public class AnimalNotFoundException extends Exception{
    public AnimalNotFoundException(String id) {
        super(id);
    }
}
