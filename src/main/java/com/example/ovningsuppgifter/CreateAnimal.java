package com.example.ovningsuppgifter;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class CreateAnimal {
    String name;
    String binomialName;

    @JsonCreator
    public CreateAnimal(@JsonProperty String name, @JsonProperty("binomialName") String binomialName) {
        this.name = name;
        this.binomialName = binomialName;
    }
}
