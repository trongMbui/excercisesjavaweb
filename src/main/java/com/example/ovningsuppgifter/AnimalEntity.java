package com.example.ovningsuppgifter;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@Data
@AllArgsConstructor
public class AnimalEntity {

    String id;
    String name;
    String binomialName;
    String description;
    String conservationStatus;

}
